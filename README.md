# staucoffea



## Getting started


## Name
This is an initial framework for Stau analysis using Run 3 NANOAOD following [bucoffea](https://github.com/bu-cms/bucoffea) framework.


## Installation

```
git clone -b stau_nano_run3 https://gitlab.cern.ch/ppalit/staucoffea.git
cd staucoffea

source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
ENVNAME="cmucoffeaenv"
python -m venv ${ENVNAME}
source ${ENVNAME}/bin/activate
export PYTHONPATH=${PWD}/${ENVNAME}/lib/python3.6/site-packages:$PYTHONPATH

pip install -r cmucoffea/requirements.txt
pip install numpy==1.19.5
pip install matplotlib==3.1
pip install mplhep==0.1.5

cd cmucoffea/cmucoffea

touch __init__.py
```

Everytime you enter the directory, you have to activate `env.sh` which will contain the following line : 

```
source /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc8-opt/setup.sh
ENVNAME="cmucoffeaenv"
DIRNAME="cmucoffea"
source ${ENVNAME}/bin/activate
export PYTHONPATH=${PWD}/${ENVNAME}/lib/python3.6/site-packages:$PYTHONPATH
export PYTHONPATH=${PWD}/${DIRNAME}/:$PYTHONPATH
export PYTHONPATH=${PWD}/${DIRNAME}/${DIRNAME}/:$PYTHONPATH
```

## Usage

`stau` folder contains the main analyzers. `stau/definitions.py` contains the declaration of histograms, trees, cutflow and nanoaod objects, while `stau/stauProcessor.py` contains the gencandidate object, object selection, event selection, filling of cutflow, histograms amd trees for both prompt and displaced analysis. 

Input parameters (cuts, trigger names, weight files etc) will be provided by yaml files in `config/`, such as, `stau_disp.yaml` for displaced analysis.

Using the processor, the local execution will be done by executor in : `scripts/run_quick.py` , which takes one displaced tau signal and DY+jets background as input samples : currently from eos. Chunksize = 10000 , better to use the whole chunk (total eventsize ) to understand while debugging.
Output is generated as cutflow in .txt file and other accumulators in .coffea file

To print the cutflow and generate histograms : 

```
./scripts/run_quick.py stau_disp(stau_prompt) 
```

Normalized plots can be printed by : `scripts/print_plots_norm.py`

```
./scripts/print_plots_norm.py {signal}.coffea {background}.coffea --region='sr_prompt'/'sr_displaced' --outpath='./out'
``` 


## Support
In case you face problem in pip installing like this : `_NamespacePath object has no attribute sort`, you may take help from this solution : 
`https://github.com/pypa/setuptools/issues/885`


